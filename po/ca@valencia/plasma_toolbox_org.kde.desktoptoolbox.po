# Translation of plasma_toolbox_org.kde.desktoptoolbox.po to Catalan (Valencian)
# Copyright (C) 2014-2024 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015.
# Josep M. Ferrer <txemaq@gmail.com>, 2019, 2020, 2021, 2022, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-26 00:39+0000\n"
"PO-Revision-Date: 2024-03-26 10:23+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 22.12.3\n"

#: contents/ui/ToolBoxContent.qml:282
#, kde-format
msgid "Global Themes"
msgstr "Temes globals"

#: contents/ui/ToolBoxContent.qml:289
#, kde-format
msgid "Display Configuration"
msgstr "Configureu la pantalla"

#: contents/ui/ToolBoxContent.qml:310
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "Més"

#: contents/ui/ToolBoxContent.qml:324
#, kde-format
msgid "Exit Edit Mode"
msgstr "Ix del mode d'edició"
