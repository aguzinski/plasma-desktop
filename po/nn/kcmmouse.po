# Translation of kcmmouse to Norwegian Nynorsk
#
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 2003, 2004, 2005, 2006.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-11 00:39+0000\n"
"PO-Revision-Date: 2024-03-17 09:26+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 24.02.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Gaute Hvoslef Kvalnes,Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "gaute@verdsveven.com,karl@huftis.org"

#: backends/kwin_wl/kwin_wl_backend.cpp:64
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""
"Feil ved søk etter inn-einingar. Prøv å opna denne innstillingsmodulen på "
"nytt."

#: backends/kwin_wl/kwin_wl_backend.cpp:88
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr "Kritisk feil ved lesing av fundamental einingsinfo for %1."

#: libinput_config.cpp:85
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""
"Feil ved innlesing av data. Sjå loggen for meir informasjon, og prøv å "
"starta oppsettmodulen på nytt."

#: libinput_config.cpp:90
#, kde-format
msgid "No pointer device found. Connect now."
msgstr "Fann inga peikareining. Prøv å kopla til ei eining no."

#: libinput_config.cpp:101
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""
"Klarte ikkje lagra alle endringane. Sjå loggen for meir informasjon. Prøv å "
"starta oppsettmodulen på nytt og prøv igjen."

#: libinput_config.cpp:121
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""
"Klarte ikkje lasta standardverdiane. Nokre av verdiane kunne ikkje setjast "
"til standardverdiar."

#: libinput_config.cpp:139
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""
"Klarte ikkje leggja til nyleg tilkopla eining. Prøv å kopla ho til ein gong "
"til, og start så oppsettmodulen på nytt."

#: libinput_config.cpp:155
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""
"Peikareininga er kopla frå, og innstillingsvindauget vart derfor lukka."

#: libinput_config.cpp:157
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr "Peikareininga er kopla frå. Fann ingen andre einingar."

#: ui/main.qml:44
#, kde-format
msgid "Device:"
msgstr "Eining:"

#: ui/main.qml:68
#, kde-format
msgid "General:"
msgstr "Generelt:"

#: ui/main.qml:69
#, kde-format
msgid "Device enabled"
msgstr "Eining tilgjengeleg"

#: ui/main.qml:83
#, kde-format
msgid "Accept input through this device."
msgstr "Ta imot inndata frå eininga."

#: ui/main.qml:89
#, kde-format
msgid "Left handed mode"
msgstr "Venstrehandsmodus"

#: ui/main.qml:102
#, kde-format
msgid "Swap left and right buttons."
msgstr "Byt verknad av venstre og høgre knapp."

#: ui/main.qml:109
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr "Trykk venstre og høgre knapp samtidig for midtklikk"

#: ui/main.qml:122
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""
"Trykk venstre og høgre knapp samtidig for å senda klikksignal tilsvarande "
"midtknapp."

#: ui/main.qml:126
#, kde-format
msgid ""
"Activating this setting increases mouse click latency by 50ms. The extra "
"delay is needed to correctly detect simultaneous left and right mouse clicks."
msgstr ""

#: ui/main.qml:136
#, kde-format
msgid "Pointer speed:"
msgstr "Peikarfart:"

#: ui/main.qml:209
#, kde-format
msgid "Pointer acceleration:"
msgstr "Peikarakselerasjon:"

#: ui/main.qml:225
#, kde-format
msgid "None"
msgstr "Ingen"

#: ui/main.qml:230
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr "Peikaren flyttar seg like langt som musa."

#: ui/main.qml:234
#, kde-format
msgid "Standard"
msgstr "Standard"

#: ui/main.qml:239
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr "Kor langt peikaren flyttar seg, avheng av kor fort musa vert flytta."

#: ui/main.qml:250
#, kde-format
msgid "Scrolling:"
msgstr "Rulling:"

#: ui/main.qml:251
#, kde-format
msgid "Invert scroll direction"
msgstr "Omvend rulleretning"

#: ui/main.qml:264
#, kde-format
msgid "Touchscreen like scrolling."
msgstr "Rulling som på ein peikeskjerm."

#: ui/main.qml:269
#, kde-format
msgid "Scrolling speed:"
msgstr "Rullefart:"

#: ui/main.qml:320
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "Saktare"

#: ui/main.qml:327
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "Raskare"

#: ui/main.qml:338
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr "Tilknyt fleire museknappar …"

#: ui/main.qml:383
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr "Ekstraknapp %1:"

#: ui/main.qml:410
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr "Trykk på museknappen som du vil knyta til ei handling"

#: ui/main.qml:411
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr "Trykk ny snøggtast for %1"

#: ui/main.qml:415
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Avbryt"

#: ui/main.qml:432
#, kde-format
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "Trykk på ein museknapp "

#: ui/main.qml:433
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr "Legg til binding …"

#: ui/main.qml:463
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr "Gå tilbake"
