# Translation of kcm_baloofile to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2016, 2018, 2019, 2020, 2021, 2022, 2023.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2023-07-15 14:42+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ui/main.qml:44
#, kde-format
msgid "Pause Indexer"
msgstr "Set indeksering på pause"

#: ui/main.qml:44
#, kde-format
msgid "Resume Indexer"
msgstr "Hald fram indeksering"

#: ui/main.qml:72
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Filsøk i KRunner og programstartarar vert slått av, og detaljert "
"metadatavising i KDE-program vert fjerna."

#: ui/main.qml:81
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Vil du sletta lagra indeksdata? %1 vert frigjord på disken, men viss du slår "
"på att indekseringa seinare, må heile indeksen byggjast på nytt. Viss du har "
"mange filer, kan dette ta lang tid."

#: ui/main.qml:83
#, kde-format
msgid "Delete Index Data"
msgstr "Slett indeksdata"

#: ui/main.qml:93
#, kde-format
msgid ""
"File Search helps you quickly locate your files. You can choose which "
"folders and what types of file data are indexed."
msgstr ""
"Filsøk hjelper deg å raskt finna filer basert på kva dei inneheld. Du kan "
"velja kva mapper og filtypar som skal indekserast."

#: ui/main.qml:109
#, kde-format
msgctxt "@title:group"
msgid "File indexing:"
msgstr "Filindeksering:"

#: ui/main.qml:110
#, kde-format
msgctxt "@action:check"
msgid "Enabled"
msgstr "Slått på"

#: ui/main.qml:124
#, kde-format
msgctxt "@label indexing status"
msgid "Status:"
msgstr "Status:"

#: ui/main.qml:128
#, kde-format
msgctxt "State and a percentage of progress"
msgid "%1, %2% complete"
msgstr "%1, %2 % fullført"

#: ui/main.qml:142
#, kde-format
msgctxt "@label file currently being indexed"
msgid "Currently indexing:"
msgstr "Indekserer no:"

#: ui/main.qml:153
#, kde-kuit-format
msgctxt "@info Currently Indexing"
msgid "<filename>%1</filename>"
msgstr "<filename>%1</filename>"

#: ui/main.qml:169
#, kde-format
msgctxt "@title:group"
msgid "Data to index:"
msgstr "Data som skal indekserast:"

#: ui/main.qml:171
#, kde-format
msgid "File names and contents"
msgstr "Filnamn og filinnhald"

#: ui/main.qml:184
#, kde-format
msgid "File names only"
msgstr "Berre filnamn"

#: ui/main.qml:203
#, kde-format
msgid "Hidden files and folders"
msgstr "Gøymde filer og mapper"

#: ui/main.qml:229
#, kde-format
msgctxt "@title:table Locations to include or exclude from indexing"
msgid "Locations"
msgstr "Stadar"

#: ui/main.qml:232
#, kde-format
msgctxt "@action:button"
msgid "Start Indexing a Folder…"
msgstr "Start indeksering av mappe …"

#: ui/main.qml:240
#, kde-format
msgctxt "@action:button"
msgid "Stop Indexing a Folder…"
msgstr "Stopp indeksering av mappe …"

#: ui/main.qml:300
#, kde-format
msgid "Not indexed"
msgstr "Ikkje indeksert"

#: ui/main.qml:301
#, kde-format
msgid "Indexed"
msgstr "Indeksert"

#: ui/main.qml:331
#, kde-format
msgid "Delete entry"
msgstr "Slett oppføring"

#: ui/main.qml:346
#, kde-format
msgid "Select a folder to include"
msgstr "Vel mappe å inkludera"

#: ui/main.qml:346
#, kde-format
msgid "Select a folder to exclude"
msgstr "Vel mappe å ekskludera"
